package com.example.bicirutas.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bicirutas.Ciclista.Modelo.Ciclista;
import com.example.bicirutas.R;

import java.util.List;

public class CiclistaAdapter extends RecyclerView.Adapter<CiclistaAdapter.ViewHolderCiclista> implements View.OnClickListener {
    List<Ciclista> lista;

    public CiclistaAdapter(List<Ciclista> lista){
        this.lista = lista;
    }
    private View.OnClickListener botonClick;

    @Override
    public void onClick(View v) {
        if(botonClick!= null){
            botonClick.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener onclickListener){
        this.botonClick = onclickListener;
    }

    @NonNull
    @Override
    public CiclistaAdapter.ViewHolderCiclista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ciclista, null);
        view.setOnClickListener(this);
        return new CiclistaAdapter.ViewHolderCiclista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CiclistaAdapter.ViewHolderCiclista holder, int position) {
        holder.datoCedula.setText(lista.get(position).getCedula());
        holder.datoNombres.setText(lista.get(position).getNombres());
        holder.datoApellidos.setText(lista.get(position).getApellidos());
        holder.datoDireccion.setText(lista.get(position).getDireccion());
        holder.datoCelular.setText(lista.get(position).getCelular());
        holder.datoGenero.setText(lista.get(position).getGenero());
        holder.datoEstatura.setText(lista.get(position).getEstatura());
        holder.datoEdad.setText(lista.get(position).getEdad());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderCiclista extends RecyclerView.ViewHolder {
        TextView datoCedula,datoNombres,datoApellidos,datoDireccion,datoCelular,datoGenero,datoEstatura,datoEdad;
        public ViewHolderCiclista(@NonNull View itemView) {
            super(itemView);
            datoCedula = (TextView) itemView.findViewById(R.id.lblcedulacliente);
            datoNombres = (TextView) itemView.findViewById(R.id.lblnombresclinete);
            datoApellidos = (TextView) itemView.findViewById(R.id.lblapellidoscliente);
            datoDireccion = (TextView) itemView.findViewById(R.id.lbldireccioncliente);
            datoCelular = (TextView) itemView.findViewById(R.id.lblcelularcliente);
            datoGenero = (TextView) itemView.findViewById(R.id.lblgenerocliente);
            datoEstatura = (TextView) itemView.findViewById(R.id.lblestaturacliente);
            datoEdad = (TextView) itemView.findViewById(R.id.lbledadcliente);
        }
    }
}
