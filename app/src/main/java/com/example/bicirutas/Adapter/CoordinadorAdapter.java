package com.example.bicirutas.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.R;

import java.util.List;

public class CoordinadorAdapter extends RecyclerView.Adapter<CoordinadorAdapter.ViewHolderCoordinador> implements View.OnClickListener {

    List<Coordinador> lista;

    public CoordinadorAdapter(List<Coordinador> lista){
        this.lista = lista;
    }
    private View.OnClickListener botonClick;

    @Override
    public void onClick(View v) {
        if(botonClick!= null){
            botonClick.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener onclickListener){
        this.botonClick = onclickListener;
    }

    @NonNull
    @Override
    public ViewHolderCoordinador onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coordinador, null);
        view.setOnClickListener(this);
        return new CoordinadorAdapter.ViewHolderCoordinador(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCoordinador holder, int position) {
        holder.datoCedula.setText(lista.get(position).getCedula());
        holder.datoNombres.setText(lista.get(position).getNombres());
        holder.datoApellidos.setText(lista.get(position).getApellidos());
        holder.datoDireccion.setText(lista.get(position).getDireccion());
        holder.datoCelular.setText(lista.get(position).getCelular());
        holder.datoGenero.setText(lista.get(position).getGenero());
    }

    @Override
    public int getItemCount() {
        return lista.size();

    }
    public class ViewHolderCoordinador extends RecyclerView.ViewHolder {
        TextView datoCedula,datoNombres,datoApellidos,datoDireccion,datoCelular,datoGenero;
        public ViewHolderCoordinador(@NonNull View itemView) {
            super(itemView);

            datoCedula = (TextView) itemView.findViewById(R.id.lblcedulacoordinadoritem);
            datoNombres = (TextView) itemView.findViewById(R.id.lblnombrescoordinadoritem);
            datoApellidos = (TextView) itemView.findViewById(R.id.lblapellidoscoordinadoritem);
            datoDireccion = (TextView) itemView.findViewById(R.id.lbldireccioncoordinadoritem);
            datoCelular = (TextView) itemView.findViewById(R.id.lblcelularcoordinadoritem);
            datoGenero = (TextView) itemView.findViewById(R.id.lblgenerocoordinadoritem);
        }
    }
}
