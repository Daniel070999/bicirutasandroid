package com.example.bicirutas.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bicirutas.Coordinador.Modelo.Ruta;
import com.example.bicirutas.R;

import java.util.List;

public class RutaAdapter extends RecyclerView.Adapter<RutaAdapter.ViewHolderRuta> implements View.OnClickListener {
    List<Ruta> lista;

    public RutaAdapter(List<Ruta> lista){
        this.lista = lista;
    }
    private View.OnClickListener botonClick;

    @Override
    public void onClick(View v) {
        if(botonClick!= null){
            botonClick.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener onclickListener){
        this.botonClick = onclickListener;
    }

    @NonNull
    @Override
    public RutaAdapter.ViewHolderRuta onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ruta, null);
        view.setOnClickListener(this);
        return new RutaAdapter.ViewHolderRuta(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RutaAdapter.ViewHolderRuta holder, int position) {
        holder.datoDistancia.setText(lista.get(position).getDistancia());
        holder.datoDificultad.setText(lista.get(position).getDificutad());
        holder.datoClima.setText(lista.get(position).getClima());
        holder.datoLugar.setText(lista.get(position).getLugar());
        holder.datoFecha.setText(lista.get(position).getFecha());
        holder.datoHoraInicio.setText(lista.get(position).getHorainicio());
        holder.datoHoraFinal.setText(lista.get(position).getHorafinal());

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderRuta extends RecyclerView.ViewHolder {
        TextView datoDistancia,datoDificultad,datoClima,datoLugar,datoFecha,datoHoraInicio,datoHoraFinal;
        public ViewHolderRuta(@NonNull View itemView) {
            super(itemView);
            datoDistancia = (TextView) itemView.findViewById(R.id.itemdistancialistaruta);
            datoDificultad = (TextView) itemView.findViewById(R.id.itemdificultadlistaruta);
            datoClima = (TextView) itemView.findViewById(R.id.itemclimalistaruta);
            datoLugar = (TextView) itemView.findViewById(R.id.itemlugarlistaruta);
            datoFecha = (TextView) itemView.findViewById(R.id.itemfechalistaruta);
            datoHoraInicio = (TextView) itemView.findViewById(R.id.itemhorainiciolistaruta);
            datoHoraFinal = (TextView) itemView.findViewById(R.id.itemhorafinallistaruta);

        }
    }
}
