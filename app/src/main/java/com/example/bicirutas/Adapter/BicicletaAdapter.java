package com.example.bicirutas.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bicirutas.Ciclista.Modelo.Bicicleta;
import com.example.bicirutas.R;

import java.util.List;

public class BicicletaAdapter extends RecyclerView.Adapter<BicicletaAdapter.ViewHolderBicicleta> implements View.OnClickListener {
    List<Bicicleta> lista;

    public BicicletaAdapter(List<Bicicleta> lista){
        this.lista = lista;
    }
    private View.OnClickListener botonClick;

    @Override
    public void onClick(View v) {
        if(botonClick!= null){
            botonClick.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener onclickListener){
        this.botonClick = onclickListener;
    }


    @NonNull
    @Override
    public BicicletaAdapter.ViewHolderBicicleta onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bicicleta, null);
        view.setOnClickListener(this);
        return new BicicletaAdapter.ViewHolderBicicleta(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BicicletaAdapter.ViewHolderBicicleta holder, int position) {
        holder.datochasis.setText(lista.get(position).getChasis());
        holder.datomarca.setText(lista.get(position).getMarca());
        holder.datoaro.setText(lista.get(position).getAro());
        holder.datotipo.setText(lista.get(position).getTipo());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderBicicleta extends RecyclerView.ViewHolder {
        TextView datochasis,datoaro,datomarca,datotipo;
        public ViewHolderBicicleta(@NonNull View itemView) {
            super(itemView);
            datochasis = (TextView) itemView.findViewById(R.id.lblchasisbicicletalistado);
            datoaro = (TextView) itemView.findViewById(R.id.lblarobicicletadolistado);
            datomarca = (TextView) itemView.findViewById(R.id.lblmarcabicicletalistado);
            datotipo = (TextView) itemView.findViewById(R.id.lbltipobicicletalistado);
        }
    }
}
