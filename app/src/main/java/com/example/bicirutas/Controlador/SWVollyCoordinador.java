package com.example.bicirutas.Controlador;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.bicirutas.Adapter.CoordinadorAdapter;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.Coordinador.Modelo.Ruta;
import com.example.bicirutas.ui.gallery.GalleryFragment;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class SWVollyCoordinador {
    Context context;
    //casa 192.168.2.116
    //samsung 192.168.43.103

    //definir las url del servicio web
    String host = "http://192.168.43.103/swProyecto/public/index.php/coordinador";
    String listarCoordinadoreslink = "/verTodos";
    String listarRutaslink = "/verTodasRutas";
    String buscarCoordinadorlink = "/getCoordinador";
    String modificarCoordinadorlink = "/modificarCoordinador";
    String buscarRutalink = "/misRutas";
    String nuevaRuta = "/nuevaRuta";
    String nuevoCoordinador = "/nuevoCoordinador";
    String datosRutalink = "/datosRuta";
    String eliminarRutalink = "/eliminarRuta";
    String eliminarCoordinadorlink = "/eliminarCoordinador";

    public SWVollyCoordinador(Context context) {
        this.context = context;
    }

    public void listarCoordinadores(final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(listarCoordinadoreslink);
        Log.e("url",path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("listado", response.toString());

                listener.onResponse(response);
                Log.e("listado", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);
    }
    public  void buscarCoordinador(String cedula,  final VolleyResponseListener listener) {

        String path = host.concat(buscarCoordinadorlink);
        path += "/"+cedula;
        Log.e("url", path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());

            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);

    }
    public void modificarCoordinador(Coordinador coordinador, final VolleyResponseListener listener){
        String path = host.concat(modificarCoordinadorlink);
        Log.e("dato","ur: "+ path);
        final JSONObject  object = new JSONObject();
        try {
           object.put("cedula",coordinador.getCedula());
           object.put("nombres",coordinador.getNombres());
           object.put("apellidos",coordinador.getApellidos());
           object.put("direccion",coordinador.getDireccion());
           object.put("celular",coordinador.getCelular());
           object.put("genero",coordinador.getGenero());

        }catch (Exception es){
            es.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path,object, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);
                Log.e("estado", response.toString());
                Toast.makeText(context, "Usuario modificado", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error.Response", error.toString());
            }
        });

       SingletonVolly.getInstance(context).addToRequestqueue(request);
    }
    public void AgregarRuta(Ruta ruta ,Coordinador coordinador, final VolleyResponseListener listener){
        String path = host.concat(nuevaRuta);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cedula", coordinador.getCedula());
            jsonObject.put("distancia", ruta.getDistancia());
            jsonObject.put("Niveldificultad", ruta.getDificutad());
            jsonObject.put("ProbabilidadClima", ruta.getClima());
            jsonObject.put("fecha", ruta.getFecha());
            jsonObject.put("hora_inicio", ruta.getHorainicio());
            jsonObject.put("hora_final", ruta.getHorafinal());
            jsonObject.put("lugar", ruta.getLugar());
            //jsonObject.put("coordinador_id", ruta.getCoordinadorID());
            Log.e("data",jsonObject.toString());

        }catch (Exception ex){

        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("registroruta", response.toString());
                Toast.makeText(context, "Ruta registrada", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(request);
    }
    public void AgregarCoordinador(Coordinador coordinador,Ruta ruta , final VolleyResponseListener listener){
        String path = host.concat(nuevoCoordinador);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cedula",coordinador.getCedula());
            jsonObject.put("nombres",coordinador.getNombres());
            jsonObject.put("apellidos",coordinador.getApellidos());
            jsonObject.put("direccion",coordinador.getDireccion());
            jsonObject.put("celular",coordinador.getCelular());
            jsonObject.put("genero",coordinador.getGenero());

            jsonObject.put("distancia", ruta.getDistancia());
            jsonObject.put("Niveldificultad", ruta.getDificutad());
            jsonObject.put("ProbabilidadClima", ruta.getClima());
            jsonObject.put("fecha", ruta.getFecha());
            jsonObject.put("hora_inicio", ruta.getHorainicio());
            jsonObject.put("hora_final", ruta.getHorafinal());
            jsonObject.put("lugar", ruta.getLugar());
            //jsonObject.put("coordinador_id", ruta.getCoordinadorID());
            Log.e("data",jsonObject.toString());

        }catch (Exception ex){

        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("registrocoordinador", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(request);
    }
    public void listarRutaslink(final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(listarRutaslink);
        Log.e("url",path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);
                Log.e("listado", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);
    }
    public  void buscarRuta(String cedula,  final VolleyResponseListener listener) {

        String path = host.concat(buscarRutalink);
        path += "/"+cedula;
        Log.e("url", path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());

            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);

    }

    public  void datosRuta(String id,  final VolleyResponseListener listener) {

        String path = host.concat(datosRutalink);
        path += "/"+id;
        Log.e("url", path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());

            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);

    }
    public void eliminarRuta(Ruta ruta , final VolleyResponseListener listener){
        String path = host.concat(eliminarRutalink);
        JSONObject  object = new JSONObject();
        Log.e("ruta",path);
        try {
            object.put("ruta",ruta.getRutaID());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,object,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                        Log.e("estado", response.toString());

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error al eliminar" , error.getMessage());
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }
    public void eliminarCoordinador(Coordinador coordinador, final VolleyResponseListener listener){
        String path = host.concat(eliminarCoordinadorlink);
        JSONObject  jsonObject = new JSONObject();
        Log.e("ruta",path);
        try {
            jsonObject.put("cedula",coordinador.getCedula());
            Log.e("cedula2 ",jsonObject.toString());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,jsonObject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                        Log.e("estado", response.toString());

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error al eliminar" , error.getMessage());
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }


    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }
}
