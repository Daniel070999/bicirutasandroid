package com.example.bicirutas.Controlador;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public class SWVollyLogin {
    Context context;
    // casa 192.168.2.116
    //samsung 192.168.43.103

    String host = "http://192.168.43.103/swProyecto/public/index.php/login";
    String ingresarlink = "/ingresar";

    public SWVollyLogin(Context context) {
        this.context = context;
    }

    public void Ingresar(String usuario, String clave , final SWVollyLogin.VolleyResponseListener listener){
        String path = host.concat(ingresarlink);
        JSONObject object = new JSONObject();
        Log.e("login",path);
        try {
            object.put("username",usuario);
            object.put("password",clave);
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,object,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }

    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }
}
