package com.example.bicirutas.Controlador;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class SingletonVolly {
    private RequestQueue queue;
    private Context context;
    private static SingletonVolly miInstancia;

    public SingletonVolly(Context context){
        this.context = context;
        queue = getRequestQueue();
    }

    public RequestQueue getRequestQueue(){
        if (queue == null)
            queue = Volley.newRequestQueue(context.getApplicationContext());
        return queue;
    }

    public static synchronized SingletonVolly getInstance(Context context){
        if (miInstancia == null){
            miInstancia = new SingletonVolly(context);
        }
        return miInstancia;
    }

    public <T> void addToRequestqueue(Request request){
        queue.add(request);
    }

}
