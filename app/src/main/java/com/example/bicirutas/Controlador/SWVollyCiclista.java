package com.example.bicirutas.Controlador;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.bicirutas.Ciclista.Modelo.Bicicleta;
import com.example.bicirutas.Ciclista.Modelo.Ciclista;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.Coordinador.Modelo.Ruta;

import org.json.JSONObject;

public class SWVollyCiclista {
    Context context;
    // casa 192.168.2.116
    // samsung 192.168.43.103
    String host = "http://192.168.43.103/swProyecto/public/index.php/cliente";
    String listarCiclistaslink = "/verTodos";
    String nuevoClientelink = "/nuevoCliente";
    String misBicicletaslink = "/misBicicletas";
    String nuevaBicicletalink = "/nuevaBicicleta";
    String modificarCiclistalink = "/modificarCliente";
    String eliminarCiclistalink = "/eliminarCliente";
    String eliminarBicicletalink = "/eliminarBicicleta";
    String miRutaActuallink = "/miRutaActual";
    String cancelarRutaActuallink = "/cancelarRuta";
    String elegirRutalink = "/elegirRuta";


    public SWVollyCiclista(Context context) {
        this.context = context;
    }


    public void listarCoordinadores(final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(listarCiclistaslink);
        Log.e("url",path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("listado", response.toString());

                listener.onResponse(response);
                Log.e("listado", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);
    }
    public void AgregarCiclista(Ciclista ciclista, Bicicleta bicicleta , final SWVollyCiclista.VolleyResponseListener listener){
        String path = host.concat(nuevoClientelink);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cedula",ciclista.getCedula());
            jsonObject.put("nombres",ciclista.getNombres());
            jsonObject.put("apellidos",ciclista.getApellidos());
            jsonObject.put("direccion",ciclista.getDireccion());
            jsonObject.put("celular",ciclista.getCelular());
            jsonObject.put("genero",ciclista.getGenero());
            jsonObject.put("estatura",ciclista.getEstatura());
            jsonObject.put("edad",ciclista.getEdad());

            jsonObject.put("chasis", bicicleta.getChasis());
            jsonObject.put("nro_aro", bicicleta.getAro());
            jsonObject.put("marca", bicicleta.getMarca());
            jsonObject.put("tipo", bicicleta.getTipo());
            //jsonObject.put("coordinador_id", ruta.getCoordinadorID());

        }catch (Exception ex){

        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);
                Log.e("registrociclista", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(request);
    }

    public void misBicicletas(String cedula,final SWVollyCiclista.VolleyResponseListener listener){
        String path = host.concat(misBicicletaslink);
        path += "/"+cedula;
        Log.e("url",path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);

    }

    public void AgregarBicicleta(Ciclista ciclista, Bicicleta bicicleta , final SWVollyCiclista.VolleyResponseListener listener){
        String path = host.concat(nuevaBicicletalink);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cedula", ciclista.getCedula());
            jsonObject.put("chasis", bicicleta.getChasis());
            jsonObject.put("nro_aro", bicicleta.getAro());
            jsonObject.put("marca", bicicleta.getMarca());
            jsonObject.put("tipo", bicicleta.getTipo());
        }catch (Exception ex){

        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);
                Log.e("regBicicleta", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(request);
    }
    public void modificarCiclista(Ciclista ciclista, final SWVollyCiclista.VolleyResponseListener listener){
        String path = host.concat(modificarCiclistalink);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cedula",ciclista.getCedula());
            jsonObject.put("nombres",ciclista.getNombres());
            jsonObject.put("apellidos",ciclista.getApellidos());
            jsonObject.put("direccion",ciclista.getDireccion());
            jsonObject.put("celular",ciclista.getCelular());
            jsonObject.put("estatura",ciclista.getEstatura());
            jsonObject.put("edad",ciclista.getEdad());

        }catch (Exception ex){

        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);
                Log.e("modificociclista", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error.toString());
            }
        });
        SingletonVolly.getInstance(context).addToRequestqueue(request);
    }

    public void eliminarCiclista(Ciclista ciclista, final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(eliminarCiclistalink);
        JSONObject  jsonObject = new JSONObject();
        Log.e("ruta",path);
        try {
            jsonObject.put("cedula",ciclista.getCedula());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,jsonObject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                        Log.e("estado", response.toString());

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }
    public void eliminarBicicleta(Bicicleta bicicleta, final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(eliminarBicicletalink);
        JSONObject  jsonObject = new JSONObject();
        Log.e("ruta",path);
        try {
            jsonObject.put("chasis",bicicleta.getChasis());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,jsonObject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                        Log.e("estado", response.toString());

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }
    public void miRutaActual(Bicicleta bicicleta, final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(miRutaActuallink);
        JSONObject  jsonObject = new JSONObject();
        Log.e("ruta",path);
        try {
            jsonObject.put("chasis",bicicleta.getChasis());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,jsonObject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }
    public void cancelarRutaActualSW(Bicicleta bicicleta, final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(cancelarRutaActuallink);
        JSONObject  jsonObject = new JSONObject();
        Log.e("ruta",path);
        try {
            jsonObject.put("ruta", bicicleta.getRutaID());
            jsonObject.put("chasis",bicicleta.getChasis());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,jsonObject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }

    public void elegirRuta(Ruta ruta, Bicicleta bicicleta, final SWVollyCoordinador.VolleyResponseListener listener){
        String path = host.concat(elegirRutalink);
        JSONObject  jsonObject = new JSONObject();
        Log.e("ruta",path);
        try {
            jsonObject.put("ruta", ruta.getRutaID());
            jsonObject.put("chasis",bicicleta.getChasis());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,jsonObject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        SingletonVolly.getInstance(context).addToRequestqueue(dr);

    }
    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }
}
