package com.example.bicirutas.Ciclista.Modelo;

public class Bicicleta {
    String chasis;
    String aro;
    String marca;
    String tipo;
    String rutaID;

    public Bicicleta(){

    }

    public Bicicleta(String chasis, String aro, String marca, String tipo, String rutaID) {
        this.chasis = chasis;
        this.aro = aro;
        this.marca = marca;
        this.tipo = tipo;
        this.rutaID = rutaID;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getAro() {
        return aro;
    }

    public void setAro(String aro) {
        this.aro = aro;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getRutaID() {
        return rutaID;
    }

    public void setRutaID(String rutaID) {
        this.rutaID = rutaID;
    }
}
