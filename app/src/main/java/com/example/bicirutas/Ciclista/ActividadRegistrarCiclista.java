package com.example.bicirutas.Ciclista;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bicirutas.Ciclista.Modelo.Bicicleta;
import com.example.bicirutas.Ciclista.Modelo.Ciclista;
import com.example.bicirutas.Controlador.SWVollyCiclista;
import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Ciclista.AcitividadCiclista;
import com.example.bicirutas.R;

import org.json.JSONObject;

public class ActividadRegistrarCiclista extends AppCompatActivity {
    EditText cajanombres,cajaapellidos,cajacedula,cajadireccion,cajacelular,cajaedad,cajaestatura,
            cajachasis,cajaaro,cajamarca;
    TextView cajagenero,
            cajatipo;
    Button botonregistrar;
    Spinner genero,tipo;
    SWVollyCiclista sw = new SWVollyCiclista(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_registrar_ciclista);
        cargarComponentes();
    }

    private void cargarComponentes() {
        cajacedula = findViewById(R.id.txtcedulaciclista);
        cajanombres = findViewById(R.id.txtnombrescliente);
        cajaapellidos = findViewById(R.id.txtapellidoscliente);
        cajadireccion = findViewById(R.id.txtdireccionciclista);
        cajacelular = findViewById(R.id.txtcelularciclista);
        cajaedad = findViewById(R.id.txtedadciclista);
        cajaestatura = findViewById(R.id.txtestaturaciclista);
        cajagenero = findViewById(R.id.lblrespuestagenero);

        genero = findViewById(R.id.spgenerociclista);
        genero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajagenero.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<CharSequence> adaptergenero = ArrayAdapter.createFromResource(this,R.array.genero,R.layout.support_simple_spinner_dropdown_item);
        genero.setAdapter(adaptergenero);

        cajachasis = findViewById(R.id.txtchasisbicicleta);
        cajaaro = findViewById(R.id.txtarobicicleta);
        cajamarca = findViewById(R.id.txtmarcabicicleta);
        cajatipo = findViewById(R.id.lblrespuestatipobicicleta);

        tipo = findViewById(R.id.sptipobicicleta);
        tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajatipo.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<CharSequence> adaptertipo = ArrayAdapter.createFromResource(this,R.array.tipobicicleta,R.layout.support_simple_spinner_dropdown_item);
        tipo.setAdapter(adaptertipo);

        botonregistrar = findViewById(R.id.btnregistrarciclista);
        botonregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ciclista ciclista = new Ciclista();
                ciclista.setCedula(cajacedula.getText().toString());
                ciclista.setNombres(cajanombres.getText().toString());
                ciclista.setApellidos(cajaapellidos.getText().toString());
                ciclista.setDireccion(cajadireccion.getText().toString());
                ciclista.setCelular(cajacelular.getText().toString());
                ciclista.setGenero(cajagenero.getText().toString());
                ciclista.setEdad(cajaedad.getText().toString());
                ciclista.setEstatura(cajaestatura.getText().toString());

                Bicicleta bicicleta = new Bicicleta();
                bicicleta.setChasis(cajachasis.getText().toString());
                bicicleta.setAro(cajaaro.getText().toString());
                bicicleta.setMarca(cajamarca.getText().toString());
                bicicleta.setTipo(cajatipo.getText().toString());

                sw.AgregarCiclista(ciclista, bicicleta, new SWVollyCiclista.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Log.e("error al guardar", message);
                        Toast.makeText(ActividadRegistrarCiclista.this, "Asegurese de llenar los campos correctamente", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(ActividadRegistrarCiclista.this, "Ciclista registrado", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ActividadRegistrarCiclista.this,AcitividadCiclista.class);
                        startActivity(intent);
                    }
                });
            }
        });
    }

}
