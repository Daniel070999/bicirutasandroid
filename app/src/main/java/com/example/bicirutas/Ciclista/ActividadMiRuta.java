package com.example.bicirutas.Ciclista;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bicirutas.Adapter.CiclistaAdapter;
import com.example.bicirutas.Adapter.RutaAdapter;
import com.example.bicirutas.Ciclista.Modelo.Bicicleta;
import com.example.bicirutas.Ciclista.Modelo.Ciclista;
import com.example.bicirutas.Controlador.SWVollyCiclista;
import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Coordinador.ActividadVerRutasCoordinador;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.Coordinador.Modelo.Ruta;
import com.example.bicirutas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadMiRuta extends AppCompatActivity {
    TextView cajacoordinador, cajafecha,cajahorainicio,cajahorafinal,cajalugar,cajaclima,cajadificultad,cajadistancia,cajachasis;
    RecyclerView recyclerView;
    Button botonlistar, botoncancelarruta;
    List<Ruta> listaRuta;
    RutaAdapter adapter;
    List<Coordinador> listaCoordinador;
    SWVollyCiclista sw = new SWVollyCiclista(this);
    SWVollyCoordinador swc = new SWVollyCoordinador(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_mi_ruta);
        cargarComponentes();
    }

    private void cargarComponentes() {
        cajachasis = findViewById(R.id.lblchasisrutaseleccionada);
        Bundle bundle = this.getIntent().getExtras();
        cajachasis.setText(bundle.getString("chasis"));

        cajacoordinador = findViewById(R.id.lblnombrescoordinadorrutaseleccionada);
        cajafecha = findViewById(R.id.lblfecharutaseleccionada);
        cajahorainicio = findViewById(R.id.lblhorainiciorutaseleccionada);
        cajahorafinal = findViewById(R.id.lblhorafinalrutaseleccionada);
        cajalugar = findViewById(R.id.lbllugarrutaseleccionada);
        cajaclima = findViewById(R.id.lblclimarutaseleccionada);
        cajadificultad = findViewById(R.id.lbldificultadrutaseleccionada);
        cajadistancia = findViewById(R.id.lbldistanciarutaseleccionada);

        cargarMiRutaSeleccionada();

        recyclerView = findViewById(R.id.recyclerlistarrutasdisponibles);

        botonlistar = findViewById(R.id.btnlistarrutasdisponibles);
        botonlistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarRutasDisponibles();
            }
        });
        botoncancelarruta = findViewById(R.id.btncancelarrutarutaactual);
        botoncancelarruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarRutaActual();
            }
        });
    }

    private void cancelarRutaActual() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Cancelar ruta actual?").setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String chasis = cajachasis.getText().toString();
                Bicicleta bicicleta = new Bicicleta();
                bicicleta.setChasis(chasis);
                sw.cancelarRutaActualSW(bicicleta, new SWVollyCoordinador.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Log.e("errorcancelarruta", message);
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(ActividadMiRuta.this, "Ruta cancelada", Toast.LENGTH_SHORT).show();
                        LimpiarCajas();
                    }
                });
                Toast.makeText(ActividadMiRuta.this, "ruta cancelada", Toast.LENGTH_SHORT).show();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ActividadMiRuta.this, "Sin cambios", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void LimpiarCajas() {
        cajacoordinador.setText("");
        cajafecha.setText("");
        cajadistancia.setText("");
        cajadificultad.setText("");
        cajaclima.setText("");
        cajahorainicio.setText("");
        cajahorafinal.setText("");
        cajalugar.setText("");
    }

    private void cargarMiRutaSeleccionada() {
        Bicicleta bicicleta = new Bicicleta();
        bicicleta.setChasis(cajachasis.getText().toString());
        Log.e("chasis",cajachasis.getText().toString());
        sw.miRutaActual(bicicleta, new SWVollyCoordinador.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Toast.makeText(ActividadMiRuta.this, "Selecciona primero una ruta", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(JSONObject response) {
                Log.e("respuesta", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    for (int i = 0 ; i < jsonArray.length() ; i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        cajafecha.setText(jsonObject.getString("fecha"));
                        cajadificultad.setText(jsonObject.getString("Niveldificultad"));
                        cajaclima.setText(jsonObject.getString("ProbabilidadClima"));
                        cajahorainicio.setText(jsonObject.getString("hora_inicio"));
                        cajahorafinal.setText(jsonObject.getString("hora_final"));
                        cajalugar.setText(jsonObject.getString("lugar"));
                        cajadistancia.setText(jsonObject.getString("distancia"));

                    }
                   JSONObject jsonObject = response.getJSONObject("datados");
                    cajacoordinador.setText(jsonObject.getString("nombres")+" "+jsonObject.getString("apellidos"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error al listar", e.getMessage());
                }
            }
        });
    }

    private void cargarRutasDisponibles() {
        swc.listarRutaslink(new SWVollyCoordinador.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Log.e("error al listar", message);
                Toast.makeText(ActividadMiRuta.this, "Asegurese de tener conexion a internet", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(final JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    listaRuta = new ArrayList<Ruta>();
                    for (int i = 0 ; i < jsonArray.length() ; i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        final Ruta ruta= new Ruta();
                        final String id = jsonObject.getString("coordinador_id");

                        ruta.setDistancia(jsonObject.getString("distancia"));
                        ruta.setDificutad(jsonObject.getString("Niveldificultad"));
                        ruta.setClima(jsonObject.getString("ProbabilidadClima"));
                        ruta.setLugar(jsonObject.getString("lugar"));
                        ruta.setFecha(jsonObject.getString("fecha"));
                        ruta.setHorainicio(jsonObject.getString("hora_inicio"));
                        ruta.setHorafinal(jsonObject.getString("hora_final"));
                        ruta.setCoordinadorID(jsonObject.getString("coordinador_id"));
                        ruta.setRutaID(jsonObject.getString("ruta_id"));

                        listaRuta.add(ruta);
                        adapter = new RutaAdapter(listaRuta);
                        recyclerView.setLayoutManager(new LinearLayoutManager(ActividadMiRuta.this));
                        adapter.setOnclickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                seleccionRuta(v);
                            }
                        });
                        recyclerView.setAdapter(adapter);
                        Log.e("rutas: ", listaRuta.toString());

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error al listarrutas", e.getMessage());
                }
            }
        });

    }

    private void seleccionRuta(final View view) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setMessage("Asistir a la ruta seleccionada? ").setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String rutaID = listaRuta.get(recyclerView.getChildAdapterPosition(view)).getRutaID();
                        String chasisB = cajachasis.getText().toString();
                        Ruta ruta = new Ruta();
                        ruta.setRutaID(rutaID);

                        Bicicleta bicicleta = new Bicicleta();
                        bicicleta.setChasis(chasisB);

                        sw.elegirRuta(ruta, bicicleta, new SWVollyCoordinador.VolleyResponseListener() {
                            @Override
                            public void onError(String message) {
                                Log.e("error elegirruta", message);
                            }

                            @Override
                            public void onResponse(JSONObject response) {
                                String verificar = "";
                                try {
                                verificar = response.getString("succes");
                                if(verificar == "false"){
                                    Toast.makeText(ActividadMiRuta.this, "Usted ya cuenta con una ruta", Toast.LENGTH_SHORT).show();
                                }else if(verificar == "true") {
                                    Toast.makeText(ActividadMiRuta.this, "Ruta asignada", Toast.LENGTH_SHORT).show();
                                    cargarMiRutaSeleccionada();
                                }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ActividadMiRuta.this, "Sin cambios", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        alertBuilder.show();
    }
}
