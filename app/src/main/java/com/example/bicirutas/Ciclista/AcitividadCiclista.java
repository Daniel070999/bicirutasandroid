package com.example.bicirutas.Ciclista;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bicirutas.Adapter.CiclistaAdapter;
import com.example.bicirutas.Ciclista.Modelo.Ciclista;
import com.example.bicirutas.Controlador.SWVollyCiclista;
import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Ciclista.AcitividadCiclista;
import com.example.bicirutas.Coordinador.ActividadCoordinador;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.security.AccessController;
import java.util.ArrayList;
import java.util.List;

public class AcitividadCiclista extends AppCompatActivity implements View.OnClickListener {

    Button botonbuscar;
    RecyclerView recyclerView;
    List<Ciclista> listaCiclista;
    CiclistaAdapter adapter;
    EditText cajaCedula;
    SWVollyCiclista sw = new SWVollyCiclista(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acitividad_ciclista);
        cargarComponentes();
        listarCiclistas();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sub_menu_ciclista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.itemregistrarciclista:
                intent = new Intent(this, ActividadRegistrarCiclista.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    private void listarCiclistas() {
        sw.listarCoordinadores(new SWVollyCoordinador.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Toast.makeText(AcitividadCiclista.this, "Asegurese de estar conectado a internet", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    listaCiclista = new ArrayList<Ciclista>();
                    for (int i = 0 ; i < jsonArray.length() ; i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Ciclista ciclista = new Ciclista();

                        ciclista.setCedula(jsonObject.getString("cedula"));
                        ciclista.setNombres(jsonObject.getString("nombres"));
                        ciclista.setApellidos(jsonObject.getString("apellidos"));
                        ciclista.setDireccion(jsonObject.getString("direccion"));
                        ciclista.setCelular(jsonObject.getString("celular"));
                        ciclista.setGenero(jsonObject.getString("genero"));
                        ciclista.setEstatura(jsonObject.getString("estatura"));
                        ciclista.setEdad(jsonObject.getString("edad"));

                        listaCiclista.add(ciclista);
                        adapter = new CiclistaAdapter(listaCiclista);
                        recyclerView.setLayoutManager(new LinearLayoutManager(AcitividadCiclista.this));
                        adapter.setOnclickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cargarDialogoOpciones(v);
                            }
                        });
                        recyclerView.setAdapter(adapter);
                        Log.e("usuarios: ", listaCiclista.toString());

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error al listar", e.getMessage());
                }
            }
        });
    }

    private void cargarDialogoOpciones(final View view) {
        final Dialog dialog = new Dialog(AcitividadCiclista.this);
        dialog.setContentView(R.layout.dialogo_opciones_ciclista);
        final TextView nombres = dialog.findViewById(R.id.lbldialogonombresciclista);
        final TextView cedula = dialog.findViewById(R.id.lbldialogocedulaciclista);
        final String cedulaBundle = listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getCedula();

        nombres.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getNombres()+" "+ listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getApellidos());
        cedula.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getCedula());

        final Button botonmisbicicletas = dialog.findViewById(R.id.btnmisbicicletas);
        botonmisbicicletas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(dialog.getContext(), ActividadMisBicicletas.class);
                Bundle bundle = new Bundle();
                bundle.putString("cedula", cedulaBundle);
                intent.putExtras(bundle);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        final Button botonelimanrciclista = dialog.findViewById(R.id.btneliminarciclista);
        botonelimanrciclista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cedula = listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getCedula();
                final Ciclista ciclista = new Ciclista();
                ciclista.setCedula(cedula);
                Log.e("cedula:" , cedula);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AcitividadCiclista.this);
                alertDialog.setMessage("Eliminar ciclista?").setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                sw.eliminarCiclista(ciclista, new SWVollyCoordinador.VolleyResponseListener() {
                                    @Override
                                    public void onError(String message) {
                                        Toast.makeText(AcitividadCiclista.this, "Asegurese de estar conectado a internet", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Toast.makeText(AcitividadCiclista.this, "Ciclista eliminado", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                        listarCiclistas();
                                    }
                                });
                            }
                        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        final Button botonmodificarciclista = dialog.findViewById(R.id.btnmodificarciclista);
        botonmodificarciclista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View viewMod) {
                final Dialog dialogMod = new Dialog(AcitividadCiclista.this);
                dialogMod.setContentView(R.layout.dialogo_modificar_ciclista);
                final EditText cajanombres = dialogMod.findViewById(R.id.txtnombresciclistamodificar);
                final EditText cajaapellidos = dialogMod.findViewById(R.id.txtapellidosciclistamodificar);
                final EditText cajadireccion = dialogMod.findViewById(R.id.txtdireccionciclistamodificar);
                final EditText cajacelular = dialogMod.findViewById(R.id.txtcelularciclistamodificar);
                final EditText cajaedad = dialogMod.findViewById(R.id.txtedadciclistamodificar);
                final EditText cajaestatura = dialogMod.findViewById(R.id.txtestaturaciclistamodificar);


                cajanombres.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getNombres());
                cajaapellidos.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getApellidos());
                cajadireccion.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getDireccion());
                cajacelular.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getCelular());
                cajaedad.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getEdad());
                cajaestatura.setText(listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getEstatura());


                Button cancelar = dialogMod.findViewById(R.id.btncancelarregistrociclista);
                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(AcitividadCiclista.this, "No se realizaron cambios", Toast.LENGTH_SHORT).show();
                        dialogMod.dismiss();
                    }
                });
                Button modificar = dialogMod.findViewById(R.id.btnmodificarciclistadialogo);
                modificar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String cedula =listaCiclista.get(recyclerView.getChildAdapterPosition(view)).getCedula();
                        String nombres = cajanombres.getText().toString();
                        String apellidos = cajaapellidos.getText().toString();
                        String direccion = cajadireccion.getText().toString();
                        String celular = cajacelular.getText().toString();
                        String edad = cajaedad.getText().toString();
                        String estatura = cajaestatura.getText().toString();

                        final Ciclista ciclista = new Ciclista();
                        ciclista.setCedula(cedula);
                        ciclista.setNombres(nombres);
                        ciclista.setApellidos(apellidos);
                        ciclista.setDireccion(direccion);
                        ciclista.setCelular(celular);
                        ciclista.setEdad(edad);
                        ciclista.setEstatura(estatura);
                        sw.modificarCiclista(ciclista, new SWVollyCiclista.VolleyResponseListener() {
                            @Override
                            public void onError(String message) {
                                Toast.makeText(AcitividadCiclista.this, "Asegurese que los campos esten correctamente llenados", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onResponse(JSONObject response) {
                                Toast.makeText(AcitividadCiclista.this, "Ciclista modificado", Toast.LENGTH_SHORT).show();
                                dialogMod.dismiss();
                                dialog.dismiss();
                                listarCiclistas();
                            }
                        });
                    }
                });
                dialogMod.show();
            }
        });

        dialog.show();
    }

    private void cargarDialogoModificarCiclista(View view) {

    }

    private void cargarComponentes() {
        recyclerView = findViewById(R.id.recyclerciclista);
        botonbuscar = findViewById(R.id.btnbuscarciclista);

        botonbuscar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnbuscarciclista:

                break;
        }
    }
}
