package com.example.bicirutas.Ciclista;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bicirutas.Adapter.BicicletaAdapter;
import com.example.bicirutas.Ciclista.Modelo.Bicicleta;
import com.example.bicirutas.Ciclista.Modelo.Ciclista;
import com.example.bicirutas.Controlador.SWVollyCiclista;
import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Coordinador.ActividadCoordinador;
import com.example.bicirutas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadMisBicicletas extends AppCompatActivity {

    TextView cedulaBundle,tipo;
    Spinner tipobicicleta;
    EditText cajachasis,cajaaro,cajamarca;
    Button botoncancelar, botonagregar;
    RecyclerView recyclerView;
    List<Bicicleta> listaBicicleta;
    BicicletaAdapter adapter;
    SWVollyCiclista sw = new SWVollyCiclista(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_mis_bicicletas);
        cargarComponentes();
        listarBicicletas();
    }

    private void listarBicicletas() {
        String cedula = cedulaBundle.getText().toString();
        sw.misBicicletas(cedula, new SWVollyCiclista.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Toast.makeText(ActividadMisBicicletas.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String dato = response.getString("succes");
                    if(dato == "false"){
                        Toast.makeText(ActividadMisBicicletas.this, "Aun no tiene bicicletas", Toast.LENGTH_SHORT).show();
                    }else if (dato == "true"){
                        JSONArray jsonArray = response.getJSONArray("data");
                        listaBicicleta = new ArrayList<Bicicleta>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Bicicleta bicicleta = new Bicicleta();

                            bicicleta.setChasis(jsonObject.getString("chasis"));
                            bicicleta.setAro(jsonObject.getString("nro_aro"));
                            bicicleta.setMarca(jsonObject.getString("marca"));
                            bicicleta.setTipo(jsonObject.getString("tipo"));

                            listaBicicleta.add(bicicleta);
                            adapter = new BicicletaAdapter(listaBicicleta);
                            recyclerView.setLayoutManager(new LinearLayoutManager(ActividadMisBicicletas.this));
                            adapter.setOnclickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    opcioneliminarbicicleta(v);
                                }
                            });
                            recyclerView.setAdapter(adapter);
                            Log.e("usuarios: ", listaBicicleta.toString());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error al listar", e.getMessage());
                }
            }
        });

    }

    private void opcioneliminarbicicleta(View view) {
        final String chasis = listaBicicleta.get(recyclerView.getChildAdapterPosition(view)).getChasis();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActividadMisBicicletas.this);
        alertDialog.setMessage("Seleccione una opcion para la bicicleta con chasis: "+chasis).setCancelable(false)
                .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        Bicicleta bicicleta = new Bicicleta();
                        bicicleta.setChasis(chasis);
                        sw.eliminarBicicleta(bicicleta, new SWVollyCoordinador.VolleyResponseListener() {
                            @Override
                            public void onError(String message) {
                                Toast.makeText(ActividadMisBicicletas.this, "Asegurese de estar conectado a internet", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onResponse(JSONObject response) {
                                Toast.makeText(ActividadMisBicicletas.this, "Bicicleta eliminada", Toast.LENGTH_SHORT).show();
                                listarBicicletas();
                            }
                        });
                    }
                }).setNegativeButton("Mi ruta", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ActividadMisBicicletas.this, ActividadMiRuta.class);
                Bundle bundle = new Bundle();
                bundle.putString("chasis", chasis);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }).setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sub_menu_nuevabicicleta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemnuevabicicleta:
                AgregarBicicleta();
                break;
        }
        return true;
    }

    private void AgregarBicicleta() {
        final Dialog dialog = new Dialog(ActividadMisBicicletas.this);
        dialog.setContentView(R.layout.dialogo_agregar_bicicleta);
        tipo = dialog.findViewById(R.id.lblrespuestatipobicicletaregistro);
        cajaaro = dialog.findViewById(R.id.txtchasisbicicletaregistro);
        cajamarca = dialog.findViewById(R.id.txtmarcabicicletaregistro);
        cajachasis = dialog.findViewById(R.id.txtchasisbicicletaregistro);
        tipobicicleta = dialog.findViewById(R.id.sptipobicicletaregistro);
        tipobicicleta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipo.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<CharSequence> adaptertipo = ArrayAdapter.createFromResource(this,R.array.tipobicicleta,R.layout.support_simple_spinner_dropdown_item);
        tipobicicleta.setAdapter(adaptertipo);
        botoncancelar = dialog.findViewById(R.id.btncancelarregistrobicicleta);
        botoncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(ActividadMisBicicletas.this, "No se guardo", Toast.LENGTH_SHORT).show();
            }
        });
        botonagregar = dialog.findViewById(R.id.btnguardarbicicletaregistro);
        botonagregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cajamarca = dialog.findViewById(R.id.txtmarcabicicletaregistro);
                cajaaro = dialog.findViewById(R.id.txtarobicicletaregistro);
                cajachasis = dialog.findViewById(R.id.txtchasisbicicletaregistro);

                String marca = cajamarca.getText().toString();
                String chasis = cajachasis.getText().toString();
                String aro = cajaaro.getText().toString();
                String tipodeB = tipo.getText().toString();

                Ciclista ciclista = new Ciclista();
                ciclista.setCedula(cedulaBundle.getText().toString());

                Bicicleta bicicleta = new Bicicleta();
                bicicleta.setChasis(chasis);
                bicicleta.setTipo(tipodeB);
                bicicleta.setAro(aro);
                bicicleta.setMarca(marca);
                sw.AgregarBicicleta(ciclista,bicicleta, new SWVollyCiclista.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(ActividadMisBicicletas.this, "error al guardar bicicleta", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(ActividadMisBicicletas.this, "Bicicleta registrada", Toast.LENGTH_SHORT).show();
                        listarBicicletas();
                        dialog.dismiss();
                    }
                });
            }
        });

        dialog.show();
    }

    private void cargarComponentes() {
        recyclerView = findViewById(R.id.recyclermisbicicletas);
        cedulaBundle = findViewById(R.id.lblcedulaciclistarecibe);

        Bundle bundle = this.getIntent().getExtras();
        cedulaBundle.setText(bundle.getString("cedula"));
    }
}
