package com.example.bicirutas.Coordinador.Modelo;

public class Coordinador {
    String coordinador_id;
    String cedula;
    String nombres;
    String Apellidos;
    String direccion;
    String celular;
    String genero;

    public Coordinador(){

    }

    public Coordinador(String coordinador_id, String cedula, String nombres, String apellidos, String direccion, String celular, String genero) {
        this.coordinador_id = coordinador_id;
        this.cedula = cedula;
        this.nombres = nombres;
        Apellidos = apellidos;
        this.direccion = direccion;
        this.celular = celular;
        this.genero = genero;
    }

    public String getCoordinador_id() {
        return coordinador_id;
    }

    public void setCoordinador_id(String coordinador_id) {
        this.coordinador_id = coordinador_id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
