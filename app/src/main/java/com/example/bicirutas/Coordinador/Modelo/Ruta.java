package com.example.bicirutas.Coordinador.Modelo;

public class Ruta {
    String distancia;
    String dificutad;
    String clima;
    String fecha;
    String horainicio;
    String horafinal;
    String coordinadorID;
    String lugar;
    String rutaID;

    public String getRutaID() {
        return rutaID;
    }

    public void setRutaID(String rutaID) {
        this.rutaID = rutaID;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getCoordinadorID() {
        return coordinadorID;
    }

    public void setCoordinadorID(String coordinadorID) {
        this.coordinadorID = coordinadorID;
    }

    public Ruta(){

}
    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getDificutad() {
        return dificutad;
    }

    public void setDificutad(String dificutad) {
        this.dificutad = dificutad;
    }

    public String getClima() {
        return clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHorainicio() {
        return horainicio;
    }

    public void setHorainicio(String horainicio) {
        this.horainicio = horainicio;
    }

    public String getHorafinal() {
        return horafinal;
    }

    public void setHorafinal(String horafinal) {
        this.horafinal = horafinal;
    }

    public Ruta(String lugar,String coordinadorID,String distancia, String dificutad, String clima, String fecha, String horainicio, String horafinal) {
        this.distancia = distancia;
        this.dificutad = dificutad;
        this.clima = clima;
        this.fecha = fecha;
        this.horainicio = horainicio;
        this.horafinal = horafinal;
        this.coordinadorID = coordinadorID;
        this.lugar = lugar;
    }
}
