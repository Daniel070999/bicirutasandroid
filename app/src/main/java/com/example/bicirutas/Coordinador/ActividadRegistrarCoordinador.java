package com.example.bicirutas.Coordinador;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.Coordinador.Modelo.Ruta;
import com.example.bicirutas.R;
import com.example.bicirutas.ui.home.HomeFragment;

import org.json.JSONObject;

public class ActividadRegistrarCoordinador extends AppCompatActivity implements View.OnClickListener {

    EditText cajanombres,cajaapellidos,cajacedula,cajadireccion,cajacelular,
                cajadistancia,cajafecha,cajahorainicial,cajahorafinal,cajalugar;
    TextView cajagenero,
                cajaclima,cajadificultad;
    Button botonregistrar;
    Spinner genero,dificultad,clima;
    SWVollyCoordinador sw = new SWVollyCoordinador(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_registrar_coordinador);
        cargarComponentes();
    }

    private void cargarComponentes() {

        cajalugar = findViewById(R.id.txtlugarregistrocoordinador);
        cajacedula = findViewById(R.id.txtcedularegistrocoordinador);
        cajanombres = findViewById(R.id.txtnombreregistrocoordinador);
        cajaapellidos = findViewById(R.id.txtapellidosregistrocoordinador);
        cajadireccion = findViewById(R.id.txtdireccioncoordinadorregistro);
        cajacelular = findViewById(R.id.txtcelularcoordinadorregistro);
        cajadistancia = findViewById(R.id.txtdistanciaregistrocoordinador);
        cajafecha = findViewById(R.id.txtfecharutaregistro);
        cajahorainicial = findViewById(R.id.txthorainiciorutaregistro);
        cajahorafinal = findViewById(R.id.txthorafinalrutaregistro);
        cajagenero = findViewById(R.id.lblrespuestagenerocoordinadorregistro);
        cajaclima = findViewById(R.id.lblrespuestaclimarutaregistro);
        cajadificultad = findViewById(R.id.lblrespuestaniveldificultadrutaregistro);
        genero = findViewById(R.id.spgenero);
        clima = findViewById(R.id.spclima);
        dificultad = findViewById(R.id.spdificultad);

        dificultad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajadificultad.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapternivel = ArrayAdapter.createFromResource(this,R.array.dificultad,R.layout.support_simple_spinner_dropdown_item);
        dificultad.setAdapter(adapternivel);

        clima.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajaclima.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapterclima = ArrayAdapter.createFromResource(this,R.array.clima,R.layout.support_simple_spinner_dropdown_item);
        clima.setAdapter(adapterclima);

        genero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajagenero.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adaptergenero = ArrayAdapter.createFromResource(this,R.array.genero,R.layout.support_simple_spinner_dropdown_item);
        genero.setAdapter(adaptergenero);

        botonregistrar = findViewById(R.id.btnregistrarcoordinadoryruta);
        botonregistrar.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Coordinador coordinador = new Coordinador();
        coordinador.setCedula(cajacedula.getText().toString());
        coordinador.setNombres(cajanombres.getText().toString());
        coordinador.setApellidos(cajaapellidos.getText().toString());
        coordinador.setDireccion(cajadireccion.getText().toString());
        coordinador.setCelular(cajacelular.getText().toString());
        coordinador.setGenero(cajagenero.getText().toString());

        Ruta ruta = new Ruta();
        ruta.setDistancia(cajadistancia.getText().toString());
        ruta.setDificutad(cajadificultad.getText().toString());
        ruta.setClima(cajaclima.getText().toString());
        ruta.setLugar(cajalugar.getText().toString());
        ruta.setFecha(cajafecha.getText().toString());
        ruta.setHorainicio(cajahorainicial.getText().toString());
        ruta.setHorafinal(cajahorafinal.getText().toString());
        sw.AgregarCoordinador(coordinador, ruta, new SWVollyCoordinador.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Toast.makeText(ActividadRegistrarCoordinador.this, "Asegurese de llenar los campos corrrectamente", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(ActividadRegistrarCoordinador.this, "Coordinador registrado", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ActividadRegistrarCoordinador.this,ActividadCoordinador.class);
                startActivity(intent);
            }
        });

    }
}
