package com.example.bicirutas.Coordinador;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bicirutas.Adapter.CoordinadorAdapter;
import com.example.bicirutas.Adapter.RutaAdapter;
import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.Coordinador.Modelo.Ruta;
import com.example.bicirutas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadVerRutasCoordinador extends AppCompatActivity implements View.OnClickListener{
    SWVollyCoordinador sw = new SWVollyCoordinador(this);
    List<Ruta> listaRuta;
    List<Coordinador> listaCoordinador;
    RecyclerView recyclerView;
    RutaAdapter adapter;
    Button botonbuscar;
    EditText cajaCedula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_ver_rutas_coordinador);
        listarRutas();
        cargarComponentes();
    }

    private void cargarComponentes() {
        recyclerView = findViewById(R.id.reclyclerlistadoderutas);
        botonbuscar = findViewById(R.id.btnbuscarrutacoordinador);
        cajaCedula = findViewById(R.id.txtcedulabuscarutacoordinador);

        botonbuscar.setOnClickListener(this);
    }

    private void listarRutas() {
        sw.listarRutaslink(new SWVollyCoordinador.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Log.e("error al listar", message);
                Toast.makeText(ActividadVerRutasCoordinador.this, "Asegurese de tener conexion a internet", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(final JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    listaRuta = new ArrayList<Ruta>();
                    for (int i = 0 ; i < jsonArray.length() ; i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        final Ruta ruta= new Ruta();
                        final String id = jsonObject.getString("coordinador_id");

                        ruta.setDistancia(jsonObject.getString("distancia"));
                        ruta.setDificutad(jsonObject.getString("Niveldificultad"));
                        ruta.setClima(jsonObject.getString("ProbabilidadClima"));
                        ruta.setLugar(jsonObject.getString("lugar"));
                        ruta.setFecha(jsonObject.getString("fecha"));
                        ruta.setHorainicio(jsonObject.getString("hora_inicio"));
                        ruta.setHorafinal(jsonObject.getString("hora_final"));
                        ruta.setCoordinadorID(jsonObject.getString("coordinador_id"));
                        ruta.setRutaID(jsonObject.getString("ruta_id"));

                        listaRuta.add(ruta);
                        adapter = new RutaAdapter(listaRuta);
                        recyclerView.setLayoutManager(new LinearLayoutManager(ActividadVerRutasCoordinador.this));
                        adapter.setOnclickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cargarDatosCoordinador(v);
                            }
                        });
                        recyclerView.setAdapter(adapter);
                        Log.e("rutas: ", listaRuta.toString());

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error al listarrutas", e.getMessage());
                }
            }
        });

    }

    private void cargarDatosCoordinador(final View view) {
        final SWVollyCoordinador sw = new SWVollyCoordinador(this);
        final Dialog dlgDetalles = new Dialog(ActividadVerRutasCoordinador.this);
        dlgDetalles.setContentView(R.layout.dialogo_datos_ruta);
        String id = listaRuta.get(recyclerView.getChildAdapterPosition(view)).getCoordinadorID();
        sw.datosRuta(id, new SWVollyCoordinador.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Log.e("errordatos", message);
            }

            @Override
            public void onResponse(JSONObject response) {
                final TextView datosCoordinador = dlgDetalles.findViewById(R.id.lbldialogocoordinadordatos);
             try {
                 JSONObject jsonObject = response.getJSONObject("data");
                 String nombres = jsonObject.getString("nombres");
                 String apllidos = jsonObject.getString("apellidos");
                 String celular = jsonObject.getString("celular");

                 datosCoordinador.setText("Coordinador: " +nombres+" "+apllidos+
                         "\n"+"Celular: "+celular);
                 Log.e("data",nombres);


            } catch (JSONException e) {
                   e.printStackTrace();
             Log.e("error al listar", e.getMessage());
             }

                final Button eliminarRuta = dlgDetalles.findViewById(R.id.btneliminarrutacoordinador);
                eliminarRuta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String rutaID = listaRuta.get(recyclerView.getChildAdapterPosition(view)).getRutaID();
                        final Ruta ruta = new Ruta();
                        ruta.setRutaID(rutaID);
                        Log.e("ruaid" , rutaID);

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActividadVerRutasCoordinador.this);
                        alertDialog.setMessage("Eliminar Ruta?").setCancelable(false)
                                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sw.eliminarRuta(ruta, new SWVollyCoordinador.VolleyResponseListener() {
                                            @Override
                                            public void onError(String message) {
                                                Toast.makeText(ActividadVerRutasCoordinador.this, "Error de servicio", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                Toast.makeText(ActividadVerRutasCoordinador.this, "Ruta eliminada", Toast.LENGTH_SHORT).show();
                                                dlgDetalles.dismiss();
                                                listarRutas();
                                            }
                                        });
                                    }
                                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alertDialog.show();
                    }
                });

            }
        });

        Button botoncancelardialogoruta = dlgDetalles.findViewById(R.id.btncancelardialogoruta);
        botoncancelardialogoruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgDetalles.dismiss();
                Toast.makeText(ActividadVerRutasCoordinador.this, "No se realizaron cambios", Toast.LENGTH_SHORT).show();
            }
        });
        dlgDetalles.show();


    }

    @Override
    public void onClick(View v) {
        try {
            String ced = cajaCedula.getText().toString();
            sw.buscarRuta(ced, new SWVollyCoordinador.VolleyResponseListener() {
                @Override
                public void onError(String message) {
                    Toast.makeText(ActividadVerRutasCoordinador.this, "No se encuentra registrado", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(final JSONObject response) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("data");
                        listaRuta = new ArrayList<Ruta>();
                        for (int i = 0 ; i < jsonArray.length() ; i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            final Ruta ruta= new Ruta();
                            final String id = jsonObject.getString("coordinador_id");
                            ruta.setDistancia(jsonObject.getString("distancia"));
                            ruta.setDificutad(jsonObject.getString("Niveldificultad"));
                            ruta.setClima(jsonObject.getString("ProbabilidadClima"));
                            ruta.setLugar(jsonObject.getString("lugar"));
                            ruta.setFecha(jsonObject.getString("fecha"));
                            ruta.setHorainicio(jsonObject.getString("hora_inicio"));
                            ruta.setHorafinal(jsonObject.getString("hora_final"));
                            ruta.setCoordinadorID(jsonObject.getString("coordinador_id"));
                            ruta.setRutaID(jsonObject.getString("ruta_id"));

                            listaRuta.add(ruta);
                            adapter = new RutaAdapter(listaRuta);
                            recyclerView.setLayoutManager(new LinearLayoutManager(ActividadVerRutasCoordinador.this));
                            adapter.setOnclickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    cargarDatosCoordinador(v);
                                }
                            });
                            recyclerView.setAdapter(adapter);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("error al listar", e.getMessage());
                    }
                }
            });
        }catch (Exception ex){
            Toast.makeText(this, "Asegurese de ingresar la cedula", Toast.LENGTH_SHORT).show();
        }

    }
}
