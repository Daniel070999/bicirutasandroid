package com.example.bicirutas.Coordinador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bicirutas.Adapter.CoordinadorAdapter;
import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.Coordinador.Modelo.Ruta;
import com.example.bicirutas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

public class ActividadCoordinador extends AppCompatActivity implements View.OnClickListener {

    Button botonbuscar;
    RecyclerView recyclerView;
    List<Coordinador> listaCoordinador;
    CoordinadorAdapter adapter;
    EditText cajaCedula;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_coordinador);
        cargarComponentes();
        listarCoordinadores();
    }

    private void cargarComponentes() {
        botonbuscar = findViewById(R.id.btnbuscarcoordinador);

        cajaCedula = findViewById(R.id.txtcedulacoordinadorbuscar);
        
        recyclerView = findViewById(R.id.recyclercoordinadoreslistado);


        botonbuscar.setOnClickListener(this);
    }

    //metodo para cargar el overflow
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sub_menu_coordinador, menu);
        return true;
    }
//metodo para la seleccion de item
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.itemregistrarcoordinador:
                intent = new Intent(this, ActividadRegistrarCoordinador.class);
                startActivity(intent);
                break;
            case R.id.itemverrutascoordinador:
                intent = new Intent(this, ActividadVerRutasCoordinador.class);
                startActivity(intent);
                break;

        }
        return true;
    }
    public void listarCoordinadores(){
        SWVollyCoordinador sw = new SWVollyCoordinador(this);
        sw.listarCoordinadores(new SWVollyCoordinador.VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Log.e("error al listar", message);
            }

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    listaCoordinador = new ArrayList<Coordinador>();
                    for (int i = 0 ; i < jsonArray.length() ; i++){
                        JSONObject usuarios = jsonArray.getJSONObject(i);
                        Coordinador usuario = new Coordinador();

                        usuario.setCedula(usuarios.getString("cedula"));
                        usuario.setNombres(usuarios.getString("nombres"));
                        usuario.setApellidos(usuarios.getString("apellidos"));
                        usuario.setDireccion(usuarios.getString("direccion"));
                        usuario.setCelular(usuarios.getString("celular"));
                        usuario.setGenero(usuarios.getString("genero"));

                        listaCoordinador.add(usuario);
                        adapter = new CoordinadorAdapter(listaCoordinador);
                        recyclerView.setLayoutManager(new LinearLayoutManager(ActividadCoordinador.this));
                        adapter.setOnclickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                cargarDialogoOpciones(v);
                            }
                        });
                        recyclerView.setAdapter(adapter);
                        Log.e("usuarios: ", listaCoordinador.toString());

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error al listar", e.getMessage());
                }
            }
        });
    }
    @Override
    public void onClick(View v) {
        final SWVollyCoordinador sw = new SWVollyCoordinador(this);
        switch (v.getId()){
            case R.id.btnbuscarcoordinador:
                try {
                    String cedula = cajaCedula.getText().toString();
                    sw.buscarCoordinador(cedula, new SWVollyCoordinador.VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                            Log.e("error", message);
                            Toast.makeText(ActividadCoordinador.this, "El coordinador no se encuentra registrado", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONArray("data");
                                listaCoordinador = new ArrayList<Coordinador>();
                                for (int i = 0 ; i < jsonArray.length() ; i++){
                                    JSONObject usuarios = jsonArray.getJSONObject(i);
                                    Coordinador usuario = new Coordinador();

                                    usuario.setCedula(usuarios.getString("cedula"));
                                    usuario.setNombres(usuarios.getString("nombres"));
                                    usuario.setApellidos(usuarios.getString("apellidos"));
                                    usuario.setDireccion(usuarios.getString("direccion"));
                                    usuario.setCelular(usuarios.getString("celular"));
                                    usuario.setGenero(usuarios.getString("genero"));

                                    listaCoordinador.add(usuario);
                                    adapter = new CoordinadorAdapter(listaCoordinador);
                                    adapter.setOnclickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            cargarDialogoOpciones(v);
                                        }
                                    });
                                    recyclerView.setLayoutManager(new LinearLayoutManager(ActividadCoordinador.this));
                                    recyclerView.setAdapter(adapter);
                                    Log.e("usuarios: ", listaCoordinador.toString());

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("error al listar", e.getMessage());
                            }
                        }
                    });
                }catch (Exception ex){
                    Toast.makeText(this, "Ingrese el numero de cedula", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private void cargarDialogoOpciones(final View view) {

        final SWVollyCoordinador sw = new SWVollyCoordinador(this);
        final Dialog dlgDetalles = new Dialog(ActividadCoordinador.this);
        dlgDetalles.setContentView(R.layout.dialogo_opciones_coordinador);


        final TextView cedulaCoordinador = dlgDetalles.findViewById(R.id.lbldialogoopcionesdatoscoodinador);
        cedulaCoordinador.setText(
                "cedula: "+listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getCedula()+
                        "\n"+"Nombres: "+listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getNombres()+
                        "\n"+"Apellidos: "+listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getApellidos());

        final Button botonopcionesmodificarcoordinador = dlgDetalles.findViewById(R.id.btndialogoopcionmidificarcoordinador);
        botonopcionesmodificarcoordinador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarDialogoModificar(view);
                dlgDetalles.dismiss();

            }
        });
        final Button botonagregarrutacoordinador = dlgDetalles.findViewById(R.id.btndialogoagregarrutacoordinador);
        botonagregarrutacoordinador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarDialogoAgregarRuta(view);
                dlgDetalles.dismiss();
            }
        });
        final Button botonelimanrcoordinador = dlgDetalles.findViewById(R.id.btndialogoopcioneseliminarcoordinador);
        botonelimanrcoordinador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cedula = listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getCedula();
                final Coordinador coordinador = new Coordinador();
                coordinador.setCedula(cedula);
                Log.e("cedula:" , cedula);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActividadCoordinador.this);
                alertDialog.setMessage("Eliminar coordinador?").setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sw.eliminarCoordinador(coordinador, new SWVollyCoordinador.VolleyResponseListener() {
                                    @Override
                                    public void onError(String message) {
                                        Toast.makeText(ActividadCoordinador.this, "Asegurese de estar conectado a internet", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Toast.makeText(ActividadCoordinador.this, "Coordinador eliminado", Toast.LENGTH_SHORT).show();
                                        dlgDetalles.dismiss();
                                        listarCoordinadores();
                                    }
                                });
                            }
                        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
        dlgDetalles.show();


    }


    private void cargarDialogoAgregarRuta(final View view) {
        final SWVollyCoordinador sw = new SWVollyCoordinador(this);
        final Dialog dialog = new Dialog(ActividadCoordinador.this);
        dialog.setContentView(R.layout.dialogo_agregar_ruta);

        final TextView niveldificultad = dialog.findViewById(R.id.lbldialogorespuestadificultad);
        final Spinner dificultad = dialog.findViewById(R.id.spdificultad);

        dificultad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                niveldificultad.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapternivel = ArrayAdapter.createFromResource(this,R.array.dificultad,R.layout.support_simple_spinner_dropdown_item);
        dificultad.setAdapter(adapternivel);
        final TextView probabilidadclima = dialog.findViewById(R.id.lbldialogoclimarespuesta);
        Spinner clima = dialog.findViewById(R.id.spclima);

        clima.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                probabilidadclima.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapterclima = ArrayAdapter.createFromResource(this,R.array.clima,R.layout.support_simple_spinner_dropdown_item);
        clima.setAdapter(adapterclima);
        Button botonguardarRuta = dialog.findViewById(R.id.btndialogorutaguardar);
        botonguardarRuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView cedulaCoordinador = dialog.findViewById(R.id.lblceduladialogoauxiliar);
                cedulaCoordinador.setText(listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getCedula());

                EditText distancia,fecha,horainicio,horafinal,lugar;

                lugar = dialog.findViewById(R.id.txtlugarrutaregistro);
                distancia = dialog.findViewById(R.id.txtdialogodistanciaruta);
                fecha= dialog.findViewById(R.id.txtdialogofecharuta);
                horainicio = dialog.findViewById(R.id.txtdialogohorainicialruta);
                horafinal = dialog.findViewById(R.id.txtdialogohorafinalruta);
                Coordinador coordinador = new Coordinador();
                coordinador.setCedula(cedulaCoordinador.getText().toString());
                Ruta ruta= new Ruta();
                ruta.setLugar(lugar.getText().toString());
                ruta.setDistancia(distancia.getText().toString());
                ruta.setDificutad(niveldificultad.getText().toString());
                ruta.setClima(probabilidadclima.getText().toString());
                ruta.setFecha(fecha.getText().toString());
                ruta.setHorainicio(horainicio.getText().toString());
                ruta.setHorafinal(horafinal.getText().toString());
                sw.AgregarRuta(ruta,coordinador, new SWVollyCoordinador.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Log.e("errorrutaguardar", message);
                        Toast.makeText(ActividadCoordinador.this, "Asegurese de llenar correctamente los datos", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(ActividadCoordinador.this, "Ruta Agregada", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
            }
        });
        Button botoncancelarruta = dialog.findViewById(R.id.btndialogocancelarruta);
        botoncancelarruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(ActividadCoordinador.this, "No se agrego la ruta", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void cargarDialogoModificar(final View view) {



        final SWVollyCoordinador sw = new SWVollyCoordinador(this);
        final Dialog dlgDetalles = new Dialog(ActividadCoordinador.this);
        dlgDetalles.setContentView(R.layout.dialogo_modificar_coordinador);


        final TextView cedulaCoordinador = dlgDetalles.findViewById(R.id.lbldialogocedulacoordinadormodificar);
        final TextView nombresCoordinador = dlgDetalles.findViewById(R.id.txtdialogonombrescoordinadormodificar);
        final TextView apellidosCoordinador = dlgDetalles.findViewById(R.id.txtdialogoapellidosmodificarcooridinador);
        final TextView direccionCoordinador = dlgDetalles.findViewById(R.id.txtdialogodireccioncoordinadormodificar);
        final TextView celularCoordinador = dlgDetalles.findViewById(R.id.txtdialogocelularcoordinadormodificar);
        cedulaCoordinador.setText(listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getCedula());
        nombresCoordinador.setText(listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getNombres());
        apellidosCoordinador.setText(listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getApellidos());
        direccionCoordinador.setText(listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getDireccion());
        celularCoordinador.setText(listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getCelular());
        Button botonmodificarcoordinador = dlgDetalles.findViewById(R.id.btndialogomodificarcoordinador);
        Button botoncancelar = dlgDetalles.findViewById(R.id.btndialogocancelarmodificacioncoordinador);
        botoncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgDetalles.dismiss();
                Toast.makeText(ActividadCoordinador.this, "No se realizaron acciones", Toast.LENGTH_SHORT).show();
            }
        });
        botonmodificarcoordinador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cedula = cedulaCoordinador.getText().toString();
                String nombres = nombresCoordinador.getText().toString();
                String apellidos = apellidosCoordinador.getText().toString();
                String direccion = direccionCoordinador.getText().toString();
                String celular = celularCoordinador.getText().toString();
                String genero = listaCoordinador.get(recyclerView.getChildAdapterPosition(view)).getGenero();
                Coordinador coordinador = new Coordinador();
                coordinador.setCedula(cedula);
                coordinador.setNombres(nombres);
                coordinador.setApellidos(apellidos);
                coordinador.setDireccion(direccion);
                coordinador.setCelular(celular);
                coordinador.setGenero(genero);
                sw.modificarCoordinador(coordinador, new SWVollyCoordinador.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        listarCoordinadores();
                    }
                });
                dlgDetalles.dismiss();
            }
        });

        dlgDetalles.show();
    }
}
