package com.example.bicirutas.ui.slideshow;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.bicirutas.Ciclista.AcitividadCiclista;
import com.example.bicirutas.Ciclista.ActividadRegistrarCiclista;
import com.example.bicirutas.Ciclista.Modelo.Bicicleta;
import com.example.bicirutas.Ciclista.Modelo.Ciclista;
import com.example.bicirutas.Controlador.SWVollyCiclista;
import com.example.bicirutas.R;
import com.example.bicirutas.ui.home.HomeFragment;

import org.json.JSONObject;

public class SlideshowFragment extends Fragment {

    EditText cajanombres,cajaapellidos,cajacedula,cajadireccion,cajacelular,cajaedad,cajaestatura,
            cajachasis,cajaaro,cajamarca;
    TextView cajagenero,
            cajatipo;
    Button botonregistrar;
    Spinner genero,tipo;
    SWVollyCiclista sw = new SWVollyCiclista(getActivity());


    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
            slideshowViewModel =
                    ViewModelProviders.of(this).get(SlideshowViewModel.class);

            View root = inflater.inflate(R.layout.activity_actividad_registrar_ciclista, container, false);

            slideshowViewModel.getText().observe(getActivity(), new Observer<String>() {
                @Override
                public void onChanged(@Nullable String s) {
                }
            });

            return root;

        }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cajacedula = (EditText) getActivity().findViewById(R.id.txtcedulaciclista);
        cajanombres = (EditText) getActivity().findViewById(R.id.txtnombrescliente);
        cajaapellidos = (EditText) getActivity().findViewById(R.id.txtapellidoscliente);
        cajadireccion = (EditText) getActivity().findViewById(R.id.txtdireccionciclista);
        cajacelular = (EditText) getActivity().findViewById(R.id.txtcelularciclista);
        cajaedad = (EditText) getActivity().findViewById(R.id.txtedadciclista);
        cajaestatura = (EditText) getActivity().findViewById(R.id.txtestaturaciclista);
        cajagenero = (TextView) getActivity().findViewById(R.id.lblrespuestagenero);

        genero = (Spinner) getActivity().findViewById(R.id.spgenerociclista);
        genero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajagenero.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<CharSequence> adaptergenero = ArrayAdapter.createFromResource(getContext(),R.array.genero,R.layout.support_simple_spinner_dropdown_item);
        genero.setAdapter(adaptergenero);

        cajachasis = (EditText) getActivity().findViewById(R.id.txtchasisbicicleta);
        cajaaro = (EditText) getActivity().findViewById(R.id.txtarobicicleta);
        cajamarca = (EditText) getActivity().findViewById(R.id.txtmarcabicicleta);
        cajatipo = (TextView) getActivity().findViewById(R.id.lblrespuestatipobicicleta);

        tipo = (Spinner) getActivity().findViewById(R.id.sptipobicicleta);
        tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajatipo.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<CharSequence> adaptertipo = ArrayAdapter.createFromResource(getContext(),R.array.tipobicicleta,R.layout.support_simple_spinner_dropdown_item);
        tipo.setAdapter(adaptertipo);

        botonregistrar = (Button) getActivity().findViewById(R.id.btnregistrarciclista);
        botonregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ciclista ciclista = new Ciclista();
                ciclista.setCedula(cajacedula.getText().toString());
                ciclista.setNombres(cajanombres.getText().toString());
                ciclista.setApellidos(cajaapellidos.getText().toString());
                ciclista.setDireccion(cajadireccion.getText().toString());
                ciclista.setCelular(cajacelular.getText().toString());
                ciclista.setGenero(cajagenero.getText().toString());
                ciclista.setEdad(cajaedad.getText().toString());
                ciclista.setEstatura(cajaestatura.getText().toString());

                Bicicleta bicicleta = new Bicicleta();
                bicicleta.setChasis(cajachasis.getText().toString());
                bicicleta.setAro(cajaaro.getText().toString());
                bicicleta.setMarca(cajamarca.getText().toString());
                bicicleta.setTipo(cajatipo.getText().toString());

                sw.AgregarCiclista(ciclista, bicicleta, new SWVollyCiclista.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Log.e("error al guardar", message);
                        Toast.makeText(getContext(), "Asegurese de llenar los campos correctamente", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getContext(), "Ciclista registrado", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), AcitividadCiclista.class);
                        startActivity(intent);
                    }
                });
            }
        });

    }
}