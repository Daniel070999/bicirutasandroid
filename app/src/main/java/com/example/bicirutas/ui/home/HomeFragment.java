package com.example.bicirutas.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.bicirutas.Coordinador.ActividadCoordinador;
import com.example.bicirutas.Ciclista.AcitividadCiclista;
import com.example.bicirutas.R;

public class HomeFragment extends Fragment implements View.OnClickListener{

    LinearLayout coordinador, ciclista;

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        coordinador = (LinearLayout) getActivity().findViewById(R.id.layoutcoordinador);
        ciclista = (LinearLayout) getActivity().findViewById(R.id.layoutcliente);

        coordinador.setOnClickListener(this);
        ciclista.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = this.getActivity().getIntent().getExtras();
        final int rol;
        rol = bundle.getInt("rollogin");
        Intent intent = null;
        switch (v.getId()) {
            case R.id.layoutcoordinador:
                if (rol == 1 ) {
                    intent = new Intent(getContext(), ActividadCoordinador.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getContext(), "No tiene permisos", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.layoutcliente:
                if(rol == 0) {
                    intent = new Intent(getContext(), AcitividadCiclista.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getContext(), "No tiene permisos", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

}