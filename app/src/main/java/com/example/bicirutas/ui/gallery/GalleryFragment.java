package com.example.bicirutas.ui.gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Coordinador.ActividadCoordinador;
import com.example.bicirutas.Coordinador.ActividadRegistrarCoordinador;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.Coordinador.Modelo.Ruta;
import com.example.bicirutas.R;

import org.json.JSONObject;

public class GalleryFragment extends Fragment{
    EditText cajanombres,cajaapellidos,cajacedula,cajadireccion,cajacelular,
            cajadistancia,cajafecha,cajahorainicial,cajahorafinal,cajalugar;
    TextView cajagenero,
            cajaclima,cajadificultad;
    Button botonregistrar;
    Spinner genero,dificultad,clima;
    SWVollyCoordinador sw = new SWVollyCoordinador(getActivity());

    private GalleryViewModel galleryViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.activity_actividad_registrar_coordinador, container, false);
        galleryViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cajalugar = (EditText) getActivity().findViewById(R.id.txtlugarregistrocoordinador);
        cajacedula = (EditText) getActivity().findViewById(R.id.txtcedularegistrocoordinador);
        cajanombres = (EditText) getActivity().findViewById(R.id.txtnombreregistrocoordinador);
        cajaapellidos = (EditText) getActivity().findViewById(R.id.txtapellidosregistrocoordinador);
        cajadireccion = (EditText) getActivity().findViewById(R.id.txtdireccioncoordinadorregistro);
        cajacelular = (EditText) getActivity().findViewById(R.id.txtcelularcoordinadorregistro);
        cajadistancia = (EditText) getActivity().findViewById(R.id.txtdistanciaregistrocoordinador);
        cajafecha = (EditText) getActivity().findViewById(R.id.txtfecharutaregistro);
        cajahorainicial = (EditText) getActivity().findViewById(R.id.txthorainiciorutaregistro);
        cajahorafinal = (EditText) getActivity().findViewById(R.id.txthorafinalrutaregistro);
        cajagenero = (TextView) getActivity().findViewById(R.id.lblrespuestagenerocoordinadorregistro);
        cajaclima = (TextView) getActivity().findViewById(R.id.lblrespuestaclimarutaregistro);
        cajadificultad = (TextView) getActivity().findViewById(R.id.lblrespuestaniveldificultadrutaregistro);
        genero = (Spinner) getActivity().findViewById(R.id.spgenero);
        clima =  (Spinner) getActivity().findViewById(R.id.spclima);
        dificultad =  (Spinner) getActivity().findViewById(R.id.spdificultad);

        dificultad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajadificultad.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapternivel = ArrayAdapter.createFromResource(getContext(),R.array.dificultad,R.layout.support_simple_spinner_dropdown_item);
        dificultad.setAdapter(adapternivel);

        clima.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajaclima.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapterclima = ArrayAdapter.createFromResource(getContext(),R.array.clima,R.layout.support_simple_spinner_dropdown_item);
        clima.setAdapter(adapterclima);

        genero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cajagenero.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adaptergenero = ArrayAdapter.createFromResource(getContext(),R.array.genero,R.layout.support_simple_spinner_dropdown_item);
        genero.setAdapter(adaptergenero);

        botonregistrar = (Button) getActivity().findViewById(R.id.btnregistrarcoordinadoryruta);
        botonregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Coordinador coordinador = new Coordinador();
                coordinador.setCedula(cajacedula.getText().toString());
                coordinador.setNombres(cajanombres.getText().toString());
                coordinador.setApellidos(cajaapellidos.getText().toString());
                coordinador.setDireccion(cajadireccion.getText().toString());
                coordinador.setCelular(cajacelular.getText().toString());
                coordinador.setGenero(cajagenero.getText().toString());

                Ruta ruta = new Ruta();
                ruta.setDistancia(cajadistancia.getText().toString());
                ruta.setDificutad(cajadificultad.getText().toString());
                ruta.setClima(cajaclima.getText().toString());
                ruta.setLugar(cajalugar.getText().toString());
                ruta.setFecha(cajafecha.getText().toString());
                ruta.setHorainicio(cajahorainicial.getText().toString());
                ruta.setHorafinal(cajahorafinal.getText().toString());
                sw.AgregarCoordinador(coordinador, ruta, new SWVollyCoordinador.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(getContext(), "Asegurese de llenar los campos corrrectamente", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getContext(), "Coordinador registrado", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), ActividadCoordinador.class);
                        startActivity(intent);
                    }
                });
            }
        });
    }

}