package com.example.bicirutas.Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bicirutas.Controlador.SWVollyCoordinador;
import com.example.bicirutas.Controlador.SWVollyLogin;
import com.example.bicirutas.Coordinador.Modelo.Coordinador;
import com.example.bicirutas.MainActivity;
import com.example.bicirutas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActividadLogin extends AppCompatActivity {

    EditText usuario,clave;
    Button ingresar;

    SWVollyLogin sw = new SWVollyLogin(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_login2);
        cargarComponentes();
    }

    private void cargarComponentes() {
        usuario = findViewById(R.id.txtusuariologin);
        clave = findViewById(R.id.txtclavelogin);
        ingresar = findViewById(R.id.btningresarlogin);
        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String usu = usuario.getText().toString();
                    String cla = clave.getText().toString();
                    sw.Ingresar(usu, cla, new SWVollyLogin.VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                            Log.e("error", message);
                            Toast.makeText(ActividadLogin.this, "Asegurese de tener conexion", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("data", response.toString());
                            try {
                                JSONObject jsonArray = response.getJSONObject("data");
                                int rol = Integer.parseInt(jsonArray.getString("is_superuser"));
                                Log.e("rol", rol+"");
                                String estado = response.getString("succes");
                                if (estado == "true"){
                                    Intent intent = new Intent(ActividadLogin.this, MainActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putInt("rollogin", rol);
                                    Log.e("rollogin", rol+"");
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                    limpiarCajas();
                                }else if(estado == "false"){
                                    Toast.makeText(ActividadLogin.this, "Sus datos son incorrectos", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(ActividadLogin.this, "Sus datos son incorrectos", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });

            }
        });
    }

    private void limpiarCajas() {
        usuario.setText("");
        clave.setText("");
    }
}
